#!/usr/bin/env python3

from rak811.rak811_v3 import Rak811
from signal import signal, SIGINT
from sys import exit
import serial
import struct
import binascii
import time

## \x22 a character
## 0x22 a numbers
## int('0x22', 16) 
## b'\xaa' bytes array
## ord(b'\xaa) =>  170
## bytes[170] => b'\xaa'

HEAD = b'\xAA'
COMMAND_ID = b'\xB4'
CMD = b'\x02'
data = b'\x00' * 12
DEVICE_ID = b'\xFF\xFF' # All devices
TAIL = b'\xAB'
CHECKSUM = sum(d for d in (CMD + data + DEVICE_ID)) & 0xFF

# PACKET = HEAD + COMMAND_ID + CMD + b'\x00'*12 + DEVICE_ID + CHECKSUM + TAIL



DEBUG =1 
SLEEP_TIME = 30 # sleeping time between measurements
#SLEEP_TIME = 180 # sleeping time between measurements
CMD_MODE = 2 # reporting mode 
CMD_QUERY_DATA = 4 # query data report measure
CMD_DEVICE_ID = 5 # set device ID
CMD_SLEEP = 6 # Sleep mode 
CMD_FIRMWARE = 7 # firmware info
CMD_WORKING_PERIOD = 8 # working period
MODE_ACTIVE = 0 # reports a measurement data in work period
MODE_QUERY = 1 # reports a measurement at query
PERIOD_CONTINUOUS = 0 # working period continious

SERIAL_PORT="/dev/ttyUSB0"
SERIAL_BAUDRATE=9600
SERIAL_TIMEOUT=2

ser = serial.Serial(port=SERIAL_PORT,
                    baudrate=SERIAL_BAUDRATE,
                    timeout=SERIAL_TIMEOUT)

def dump(var):
    print(var)

# construct_packet
# construct the packet to send
# cmd : the cmd(short) to send
# data : data(bytearray) to send
def construct_packet(cmd, data=b''):
    assert len(data) <= 12
    data += b'\x00'*(12-len(data))
    checksum = sum(bytes([cmd]) + data + DEVICE_ID) & 0xFF
    packet = HEAD + COMMAND_ID + bytes([cmd]) + data + DEVICE_ID
    packet += bytes([checksum]) + TAIL
   
    if DEBUG:
        dump(packet)

    return packet

# read_response
# read device reponse
def read_response():
    byte = 0
    while byte != b"\xaa":
        byte = ser.read(size=1)
        #print(byte)

    res = ser.read(size=9)

    if DEBUG:
        dump(byte + res)

    return byte + res

# process_data
# process device response as measurement
# data: reponse
def process_data(data):
    d = struct.unpack('<HHxxBB', data[2:])
    pm25 = d[0]/10.0
    pm10 = d[1]/10.0
    checksum = sum(v for v in data[2:8]) & 0xFF
    if checksum != d[2] : print("Error in data received (bad checksum)")
    return [pm25, pm10]

# cmd_data_get
# query and get data measurement
def cmd_get_data():
    ser.write(construct_packet(CMD_QUERY_DATA))
    d = read_response()
    values = []
    if bytes([d[1]]) == b'\xc0':
        values = process_data(d)
        print("PM 2.5: {} μg/m^3  PM 10: {} μg/m^3".format(values[0], values[1]))
    return values

# cmd_set_sleep
# set sleep mode
# sleep : 0 work, 1 sleep
def cmd_set_sleep(sleep=1):
    mode = b'\x00' if sleep else b'\x01'
    #print("Sleep mode : " + str(mode))
    ser.write(construct_packet(CMD_SLEEP, b'\x01' + mode))
    read_response()

# cmd_set_mode
# set the measurements mode
# mode : query mode 0 active, 1 query
def cmd_set_mode(mode=MODE_QUERY):
    ser.write(construct_packet(CMD_MODE, b'\x01' + bytes([mode])))
    read_response()

# cmd_set_working_period
# set the working period in minutes
# period : 1-30 minutes (and sleep 60-30 sec)
def cmd_set_working_period(period=5):
    ser.write(construct_packet(CMD_WORKING_PERIOD, b'\x01' + bytes([period])))
    read_response()

# set_id
# set the device Id
def set_id(id):
    id_h = (id>>8) % 256
    id_l = id % 256
    ser.write(construct_packet(CMD_DEVICE_ID, b'\00'*10 + [id_l,id_h]))
    read_response()

# get_firmware_ver
# get device firmware version
def get_firmware_ver():
    ser.write(construct_packet(CMD_FIRMWARE))
    res = read_response()
    r = struct.unpack('<BBBHBB', res[3:])
    checksum = sum(res[2:8])%256
    print("Y: {}, M: {}, D: {}, ID: {}, CRC={}".format(r[0], r[1], r[2], hex(r[3]), "OK" if (checksum==r[4] and r[5]==0xab) else "NOK"))


def handler(signal_received, frame):
    print("SIGINT or CTRL-C detected. Exiting gracefully")
    cmd_set_sleep(1)
    time.sleep(3)
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    ser.close()
    lora.close()
    exit(0)

# main
if __name__ == "__main__":

    signal(SIGINT, handler)
        #ser.open()
    ser.reset_input_buffer()
    cmd_set_sleep(0)
    cmd_set_mode()
    get_firmware_ver()

    lora = Rak811()
    lora.set_config('lora:work_mode:0')
    lora.set_config('lora:join_mode:0')
    lora.set_config('lora:region:EU868')
    lora.set_config('lora:app_eui:XXXX')
    lora.set_config('lora:app_key:XXXX')
    lora.join()
    lora.set_config('lora:dr:5')


    time.sleep(5)

    while True:
        cmd_set_sleep(0)
        time.sleep(3)
        values = cmd_get_data()
        lora.send(bytes.fromhex('0102{0:x}0202{1:x}'.format(int(values[0]*10),int(values[1]*10))))

        print("Sleep for " + str(SLEEP_TIME/60) + " min...")
        cmd_set_sleep(1)
        time.sleep(SLEEP_TIME)


    lora.close()
    #ser.close()
