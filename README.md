# Raspberry Pi + LoraWan + Air Sensor

> **Draft**

## Idea

https://sensor.community/fr/

## Hardware

* Raspberry Pi4 (4GB)
* Nova SDS011  USB plug
* Lora Node pHAT RAKWireless RAK811 (base on Semtech SX1276)  


![Pi4 + Lora Hat + SDS011](images/PiLoraAirSensor.jpg)
![Pi4 + Lora Hat + SDS011](images/PiLoraAirSensor2.jpg)



## Configuration

### Enable Lora pHat

Enable Serial:
```
sudo raspi-config
# -> Interface options -> P6 serial Port -> No -> Yes
sudo vi /boot/config.txt
# Add at the end
dtoverlay=pi3-miniuart-bt
```

Reboot

Now install Python library
```
sudo apt update
sudo apt upgrade
sudo apt install python3-pip
sudo pip3 install rak811
```

Get pHAT EUI to register into The Things NetWorks 
```
## Firmware v2.X
rak811 hard-reset
rak811 get-config dev_eui
## firmware V3.X
rak811v3 set-config lora:join_mode:0
rak811v3 get-config lora:status | grep devEui
```

### Nova USB plug

Just plug it in a USB port.

## Code

[air\_capture.py](air_capture.py)

Set keys for lora in *main*.
Change SLEEP\_TIME in seconds, time between air records.

## Data

* Air Dust Data *Nova SDS011* see the documentation [doc/Laser_Dust_Sensor_Control_Protocol_V1.3.pdf](doc/Laser_Dust_Sensor_Control_Protocol_V1.3.pdf)  
* Register device into https://thethings.network
* Visualisation register a **Cayenne LPP** device at https://cayenne.mydevices.com/ using the Lora pHAT DevEUI `rak811v3 get-config lora:status | grep devEui`



## References

* https://sensor.community./fr/
* https://www.raspberrypi.org/blog/monitor-air-quality-with-a-raspberry-pi/
* https://eco-sensors.ch/mesure-de-la-qualite-de-lair-pm2-5-pm10/
* https://github.com/binh-bk/nova_fitness_sds011/blob/master/sds011/__init__.py
* https://raw.githubusercontent.com/pierrot10/aqi/master/python/aqi.py
* https://learn.pi-supply.com/make/getting-started-with-the-raspberry-pi-lora-node-phat/
* http://www.wiki-rennes.fr/Les_capteurs_Ambassad%27Air
* https://www.framboise314.fr/mise-en-place-dune-passerelle-et-dun-noeud-lora/
* https://github.com/AmedeeBulle/pyrak811

